package sharing.example.demo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import sharing.example.demo.controller.DemoController;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	private DemoController demoController;
	
	@Test
	void contextLoads() {
		assertThat(demoController).isNotNull();
	}

}
