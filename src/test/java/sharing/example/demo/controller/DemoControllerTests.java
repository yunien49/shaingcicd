package sharing.example.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import sharing.example.demo.service.MathService;

@SpringBootTest
public class DemoControllerTests {
	
	@Autowired
	private MathService mathService;
	
	@Test
	public void plus() {
        assertThat(mathService.plus(2,3)).isEqualTo(5);
	}
	
	@Test
	public void multiply() 
	{
		assertThat(mathService.multiply(2, 3)).isEqualTo(6);
	}
}
