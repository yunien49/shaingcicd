package sharing.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import sharing.example.demo.service.IMathService;


@RestController
public class DemoController {

	@GetMapping("/")
	public String index() {
		return "Spring Boot , first successful. DEV";
	}
	
	@Autowired
	IMathService mathService;
	
	@GetMapping("/{first}/plus/{second}")
	public int plus(@PathVariable(value = "first") int first, @PathVariable(value = "second") int second) 
	{
		return mathService.plus(first, second);
	}
	
	@PostMapping("/{first}/multiply/{second}")
	public int multiply(@PathVariable(value = "first") int first, @PathVariable(value = "second") int second) 
	{
		return mathService.multiply(first, second);
	}
	
	
}
