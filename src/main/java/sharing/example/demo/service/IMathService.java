package sharing.example.demo.service;

public interface IMathService {
	public int plus(int first, int second);
	public int multiply(int first, int second);
}
