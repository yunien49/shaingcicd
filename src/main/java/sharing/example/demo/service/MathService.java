package sharing.example.demo.service;

import org.springframework.stereotype.Service;

@Service
public class MathService implements IMathService {

	
	@Override
	public int plus(int first, int second) 
	{
		return first+second;
	}

	@Override
	public int multiply(int first, int second) 
	{
		return first*second;
	}

}
